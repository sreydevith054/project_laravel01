<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function getData(){
        $employees = Employee::paginate(8);
    //    dd($employees);
        return view('home', compact('employees'));
      }
}
